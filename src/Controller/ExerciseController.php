<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Common\Generate;

/**
 * Exercise controller.
 * @Route("/twig")
 */
class ExerciseController extends AbstractController
{
    /**
     * @Route("/exercice1", name="exercise1")
     */
    public function exercise1(Generate $generator)
    {
        return $this->render('exercise1/index.html.twig', [
            'formers' => $generator->getFormers()
        ]);
    }

    /**
     * @Route("/exercice2", name="exercise2")
     */
    public function exercise2(Generate $generator)
    {
        return $this->render('exercise2/index.html.twig', [
            'numbers' => $generator->getNumbers(),
        ]);
    }

    /**
     * @Route("/exercice3", name="exercise3")
     */
    public function index(Generate $generator)
    {
        return $this->render('exercise3/index.html.twig', [
            'colors' => $generator->getColors(),
        ]);
    }
}
