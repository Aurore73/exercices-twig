<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 21/04/20
 * Time: 22:13
 */

namespace App\Common;

class Generate
{
    public function generateArray($base, $number)
    {
        $results = [];
        for($i = 0; $i < $number; $i++) {
            $results[] = $base[random_int(0, count($base) - 1)];
        }
        return $results;
    }

    public function getColors()
    {
        return $this->generateArray(array('green', 'red', 'blue', 'yellow'), 100);
    }

    public function getFormers()
    {
        $names = array('jérôme', 'thomas', 'fabien', 'faustine', 'pierre', 'agnès', 'stéphane', 'jean');
        return $this->generateArray($names, count($names));
    }

    public function getNumbers()
    {
        $results = [];
        for($i = 0; $i < 50; $i++) {
            $results[] = random_int(0, 100);
        }
        return $results;
    }
}