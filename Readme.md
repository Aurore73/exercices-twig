Exercices Twig
==============

# Exercice 1
* Afficher la liste des formateurs fournis
* Ajouter une majuscule sur chaque prénom
* Les trier par ordre alphabétique

# Exercice 2
* Afficher les nombres de la liste
* Afficher seulement les nombres supérieurs à 50

# Exercice 3
* Afficher dans une liste à puces (class bullets) le nom des couleurs avec le 
style correspondant défini dans common/colors.html.twig
* Si deux lignes successives ont la même couleur, remplacer le nom de la couleur
par '>'